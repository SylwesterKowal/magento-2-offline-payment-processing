<?php

namespace Kowal\OrderOfflinePayment\Plugin\Magento\Sales\Model\AdminOrder;

use Magento\Sales\Model\Order;

class Create
{
    /**
     * @var \Magento\Sales\Model\Service\InvoiceService
     */
    protected $_invoiceService;

    /**
     * @var \Magento\Framework\DB\TransactionFactory
     */
    protected $_transactionFactory;

    /**
     * Create constructor.
     * @param \Magento\Sales\Model\Service\InvoiceService $invoiceService
     * @param \Magento\Framework\DB\TransactionFactory $transactionFactory
     */
    public function __construct(
        \Magento\Sales\Model\Service\InvoiceService $invoiceService,
        \Magento\Framework\DB\TransactionFactory $transactionFactory
    )
    {
        $this->_invoiceService = $invoiceService;
        $this->_transactionFactory = $transactionFactory;
    }

    public function createInvoice($order)
    {

        try {
            if (!$order->canInvoice()) {
                return null;
            }
            if (!$order->getState() == 'new') {
                return null;
            }

            $invoice = $this->_invoiceService->prepareInvoice($order);
            $invoice->setRequestedCaptureCase(\Magento\Sales\Model\Order\Invoice::CAPTURE_ONLINE);
            $invoice->register();

            $transaction = $this->_transactionFactory->create()
                ->addObject($invoice)
                ->addObject($invoice->getOrder());

            $transaction->save();

        } catch (\Exception $e) {
            $order->addStatusHistoryComment('Exception message: ' . $e->getMessage(), false);
            $order->save();
            return null;
        }
    }

    public function afterCreateOrder(
        \Magento\Sales\Model\AdminOrder\Create $subject,
        $result
    )
    {
        $methodName = $this->getOrderPayment($result);

        $methods = ['offline_payment_credit_card', 'offline_payment_cash', 'offline_payment_terminal', 'offline_payment_bank_transfer', 'adminonly_paymentmethod'];
        if (in_array($methodName, $methods)) {
            $this->createInvoice($result);
            $this->changeStatus($result);
        }


        return $result;
    }

    private function changeStatus($order)
    {
        $orderState = Order::STATE_PROCESSING;
        $order->setState($orderState)->setStatus(Order::STATE_PROCESSING);
        $order->save();
    }

    private function getOrderPayment($order)
    {
        $payment = $order->getPayment();
        $method = $payment->getMethodInstance();
        $methodName = $payment->getMethod();
        $methodTitle = $method->getTitle();
        return $methodName;
    }
}